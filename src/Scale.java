import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.util.leap.ArrayList;
import jade.util.leap.List;
import org.json.JSONObject;
import com.fazecast.jSerialComm.SerialPort;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Scanner;


/**
 * @author Daryl Antonio Nelson 12/10/1996
 */

public class Scale extends Agent {

    ACLMessage cAgents;
    double scale_value = 2;
    JSONObject sensor_data = new JSONObject();
    static SerialPort chosenPort;


    @Override
    protected void setup(){

        //register agent service
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("Scale-1");
        sd.setName(getLocalName());
        dfd.addServices(sd);

        try{DFService.register(this, dfd);}
        catch(FIPAException fe){}

        //setting up message

        //message.put("grams", scale_value);
        cAgents = new ACLMessage(ACLMessage.INFORM);
        cAgents.addReceiver(new AID ("Manager", AID.ISLOCALNAME));


        // populate the drop-down box
        SerialPort[] portNames = SerialPort.getCommPorts();
        for(int i = 0; i < portNames.length; i++){
            portNames[i].getSystemPortName();
        }

        List list = new ArrayList();
        for (SerialPort port : portNames) {
//            System.out.println(port.getSystemPortName());
            list.add(port.getSystemPortName());
        }

        System.out.println(list.get(0));

        chosenPort = SerialPort.getCommPort(list.get(0).toString());
        chosenPort.setComPortTimeouts(SerialPort.TIMEOUT_SCANNER, 0, 0);


        System.out.println(portNames);









        //agent Behaviour
        addBehaviour(new CyclicBehaviour(){

                @Override
                public void action(){

                    DateFormat format = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss.SSS" );

                    sensor_data.put("sensor", 1);
                    sensor_data.put("metric", "moisture percentage");

                    if(chosenPort.openPort()) {
                        Scanner scanner = new Scanner(chosenPort.getInputStream());
                        while(scanner.hasNextLine()) {
                            try {
                                long timestamp = System.currentTimeMillis();
                                String time = format.format( timestamp );
                                sensor_data.put("time", time);
                                String line = scanner.nextLine();
                                double number = Integer.parseInt(line);
                                sensor_data.put("measurement", number);
                                try {
                                    FileWriter writer = new FileWriter("Scale.txt", true);
                                    writer.write(sensor_data+"");
                                    writer.write(",\n");
                                    writer.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                try{
                                    Thread.sleep(1);
                                }
                                catch(InterruptedException ex)
                                {
                                    Thread.currentThread().interrupt();
                                }

//                                cAgents.setContent(sensor_data.toString());
//
//                                send(cAgents);

                            } catch(Exception e) {}
                        }
                        scanner.close();
                    }


            }
        });
    }
}
