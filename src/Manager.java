import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Daryl Antonio Nelson 12/10/1996
 */

public class Manager extends Agent{

    JSONObject message = new JSONObject();
    ACLMessage received_message;
    String message_string;
    Integer message_integer;
    Integer previous_message_integer = 0;
    ACLMessage cAgents;


    @Override
    protected void setup(){

        //register agent service
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("Manager");
        sd.setName(getLocalName());
        dfd.addServices(sd);


        try{DFService.register(this, dfd );}
        catch(FIPAException fe){}

        cAgents = new ACLMessage(ACLMessage.INFORM);
        cAgents.addReceiver(new AID ("Database", AID.ISLOCALNAME));

        //agent Behaviour
        addBehaviour(new CyclicBehaviour(){

            @Override
            public void action(){

//                received_message = null;
//                received_message = receive();
//
//                if(received_message != null){
//                    System.out.println("recieved message "+ received_message);

//                    message_string = received_message.getContent();

//                    try {
//                        message_integer = Integer.parseInt(message_string);
//                    }
//                    catch (NumberFormatException e)
//                    {
//                        message_integer = 0;
//                    }

//                    System.out.println("message "+ message_string);


                try {
                    try{
                        Thread.sleep(10000);
                    }
                    catch(InterruptedException ex)
                    {
                        Thread.currentThread().interrupt();
                    }
                    Scanner input = new Scanner(new File("Scale.txt"));
                    input.useDelimiter("\n");
//                    System.out.println("file: "+ input);
                    StringBuilder sb = new StringBuilder();
                    while(input.hasNext()) {
//                        System.out.println("val "+ input.next().toString());
                        sb.append(input.next());
                    }
                    input.close();
                    sb.deleteCharAt(sb.length()-1);
                    sb.insert(0, "[");
                    sb.append("]");
//                    System.out.println("val "+ sb.toString());

                    PrintWriter writer = new PrintWriter("Scale.txt");
                    writer.print("");
                    writer.close();

                    cAgents.setContent(sb.toString());
                    send(cAgents);

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }







//                }else{
//
//                    block();
//
//                }
            }
        });

    }

}




