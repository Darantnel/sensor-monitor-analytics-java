#!/bin/bash
From openjdk:12-alpine
COPY . /var/www/java
WORKDIR /var/www/java
ENV DISPLAY=:1.0
RUN ls
CMD ["java","-classpath","/var/www/java/build/classes/jade.jar"]
#ENTRYPOINT ["java","-jar","/var/www/java/out/artifacts/SmartHive_jar/SmartHive.jar"]
EXPOSE 8001