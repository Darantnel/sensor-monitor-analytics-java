import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;
import jade.core.AID;
import javax.swing.JOptionPane;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import javax.swing.JFrame;


import org.json.JSONObject;
import com.mongodb.MongoClient;

/**
 * @author Daryl Antonio Nelson 6/1/2020
 */

public class Database extends Agent {

    int randomNum = ThreadLocalRandom.current().nextInt(500, 2499 + 1);
    JSONObject message = new JSONObject();
    ACLMessage received_message;
    String message_string;
    BasicDBObject dbObject = new BasicDBObject();
    List<String> dbnames;

    @Override
    protected void setup(){

        //register agent service
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("Database");
        sd.setName(getLocalName());
        dfd.addServices(sd);

        try{DFService.register(this, dfd);}
        catch(FIPAException fe){}

        //Database Object
        BasicDBObject sensor = new BasicDBObject();

        //agent Behaviour
        addBehaviour(new CyclicBehaviour(){
            @Override
            public void action(){

                received_message = null;
                received_message = receive(); //receives a message from master

                Socket clientSocket = null;
                try {
                    clientSocket = new Socket("localhost", 8000);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println(clientSocket);

                if(received_message != null){

                    try {
                        //HTTP request
                        URL url = null;
                        try {
                            url = new URL("http://localhost:8000/api/add_values");
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                        URLConnection con = null;
                        try {
                            con = url.openConnection();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        HttpURLConnection http = (HttpURLConnection)con;
                        try {
                            http.setRequestMethod("POST"); // PUT is another valid option
                        } catch (ProtocolException e) {
                            e.printStackTrace();
                        }
                        http.setDoOutput(true);

                        message_string = received_message.getContent();

                        System.out.println("recieved message content database "+ message_string);

                        byte[] out = message_string.getBytes(StandardCharsets.UTF_8);
                        int length = out.length;

                        http.setFixedLengthStreamingMode(length);
                        http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                        http.setRequestProperty("Authorization", "Token 411ded84657df6c5cf08493a1ad044ef91950696");
                        try {
                            http.connect();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        try(OutputStream os = http.getOutputStream()) {
                            os.write(out);

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        received_message = null;

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }else{

                    block();

                }
            }
        });
    }
}
